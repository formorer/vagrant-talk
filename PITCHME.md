## Heisser Scheiss mit vagrant
### Alexander Wirt <alexander.wirt@credativ.de>

---

# Provisioning
### Shell (inline)

```ruby
$script = <<SCRIPT
echo I am provisioning...
date > /etc/vagrant_provisioned_at
SCRIPT

Vagrant.configure("2") do |config|
  config.vm.provision "shell", inline: $script
end
```

---

### Shell (script)

```ruby
Vagrant.configure("2") do |config|
  config.vm.provision "shell", path: "script.sh"
end
```

---

## Shell (external)

```ruby
Vagrant.configure("2") do |config|
  config.vm.provision "shell", path: "https://example.com/provisioner.sh"
end
```

---

## Puppet

```ruby
Vagrant.configure("2") do |config|
  config.vm.provision "puppet"
end
```

### Dateistruktur

```shell
.
├── manifests
│   └── default.pp
└── Vagrantfile
```

---

### Manifest

```puppet
exec { 'apt-get update':
     command => '/usr/bin/apt-get update'
}

package { 'hello':
  ensure => installed,
  require => Exec['apt-get update']
}
```

---

## Ansible

```ruby
  config.vm.provision "ansible" do |ansible|
    ansible.playbook = "playbook.yml"
  end
```

---

### Playbook

```yml
- hosts: all
  remote_user: debian
  sudo: yes
  tasks:
    - name: install hello
      apt:
        name: hello
        update_cache: yes
```

---
# Mehrere Hosts

```ruby
Vagrant.configure("2") do |config|
  config.vm.provision "shell", inline: "echo Hello"

  config.vm.define "web" do |web|
    web.vm.box = "apache"
  end

  config.vm.define "db" do |db|
    db.vm.box = "mysql"
  end
end
```

---

## Kommandos
```shell
# vagrant ssh apache
# vagrant provision apache
```

---

## Default machine

```ruby
config.vm.define "web", primary: true do |web|
  # ...
end
```

## Autostart
```ruby
config.vm.define "web"
config.vm.define "db"
config.vm.define "db_follower", autostart: false
```

---

## the lazy way
### it is ruby!

```ruby
Vagrant.configure("2") do |config|
  (1..2).each do |i|
    id = "#{i}"
    config.vm.define "db#{id}" do |db|
    db.vm.network "private_network", ip: "192.168.51.1#{id}"
    db.vm.hostname = "db#{id}"
  end
end
```

---

### multi ansible

```ruby
  last.vm.provision :ansible do |ansible|
    # Disable default limit to connect to all the machines
    ansible.limit = "all"
    ansible.playbook = "playbook.yml"
  end
```

---

### even better

```ruby
Vagrant.configure("2") do |config|
  ansible_inventory_dir = "ansible/hosts"

  config.vm.provision "ansible" do |ansible|
    ansible.playbook = "ansible/playbook.yml"
    ansible.inventory_path = "#{ansible_inventory_dir}/vagrant"
    ansible.limit = 'all'
  end

  # setup the ansible inventory file
  Dir.mkdir(ansible_inventory_dir) unless Dir.exist?(ansible_inventory_dir)
  File.open("#{ansible_inventory_dir}/vagrant" ,'w') do |f|
    f.write "[#{settings['vm_name']}]\n"
    f.write "#{settings['ip_address']}\n"
  end
end
```


---
### still ruby

```yml
ip_address: 192.168.33.66
vm_name: example
server_domain: example.dev
```

```ruby
require 'yaml'
settings = YAML.load_file 'vagrant.yml'

Vagrant.configure("2") do |config|
  config.vm.network :private_network, ip: settings['ip_address']
end
```
---

# Plugins
<span style="font-size:0.6em; color:gray">https://github.com/hashicorp/vagrant/wiki/Available-Vagrant-Plugins</span>

* vagrant-hostmanager
* vagrant-openstack
* vagrant-auto_network
* vagrant-libvirt